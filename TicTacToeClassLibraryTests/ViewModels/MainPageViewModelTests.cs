﻿using NUnit.Framework;
using System.Drawing;
using TicTacToeClassLibrary.Models;

namespace TicTacToeClassLibrary.ViewModels.Tests
{
    [TestFixture]
    public class MainPageViewModelTests
    {
        [SetUp]
        public void SetupTests()
        {
            mainPage = new MainPageViewModel
            {
                NumberOfRowsAndColumns = 3,
                PointsToWin = 3
            };
        }

        private MainPageViewModel mainPage;

        [Test]
        public void CheckIfWinTest()
        {
            string[,] testMatrix;
            testMatrix = new[,]
            {
                {"", "x", ""},
                {"o", "x", "o"},
                {"x", "o", "x"}
            };

            mainPage.Collection.Clear();
            foreach (string item in testMatrix)
            {
                mainPage.Collection.Add(new CellModel { Text = item, ButtonColorBrush = Color.AliceBlue });
            }

            Assert.IsFalse(mainPage.CheckIfWin("x", mainPage.CollectionToArray(mainPage.Collection)));
        }

        [Test]
        public void CheckIfWinTestDiagonal()
        {
            string[,] testMatrix;
            testMatrix = new[,]
            {
                {"x", "", ""},
                {"", "x", ""},
                {"", "", "x"}
            };

            mainPage.Collection.Clear();
            foreach (string item in testMatrix)
            {
                mainPage.Collection.Add(new CellModel { Text = item, ButtonColorBrush = Color.AliceBlue });
            }

            Assert.IsTrue(mainPage.CheckIfWin("x", mainPage.CollectionToArray(mainPage.Collection)));
        }

        [Test]
        public void CheckIfWinTestDiagonal2()
        {
            string[,] testMatrix;
            testMatrix = new[,]
            {
                {"", "", "x"},
                {"", "x", ""},
                {"x", "", ""}
            };

            mainPage.Collection.Clear();
            foreach (string item in testMatrix)
            {
                mainPage.Collection.Add(new CellModel { Text = item, ButtonColorBrush = Color.AliceBlue });
            }

            Assert.IsTrue(mainPage.CheckIfWin("x", mainPage.CollectionToArray(mainPage.Collection)));
        }

        [Test]
        public void CheckIfWinTestHorizontal()
        {
            string[,] testMatrix;
            testMatrix = new[,]
            {
                {"x", "x", "x"},
                {"", "", ""},
                {"", "", ""}
            };

            mainPage.Collection.Clear();
            foreach (string item in testMatrix)
            {
                mainPage.Collection.Add(new CellModel { Text = item, ButtonColorBrush = Color.AliceBlue });
            }

            Assert.IsTrue(mainPage.CheckIfWin("x", mainPage.CollectionToArray(mainPage.Collection)));
        }


        [Test]
        public void CheckIfWinTestTie()
        {
            string[,] testMatrix;
            testMatrix = new[,]
            {
                {"o", "x", "o"},
                {"o", "x", "o"},
                {"x", "o", "x"}
            };

            mainPage.Collection.Clear();
            foreach (string item in testMatrix)
            {
                mainPage.Collection.Add(new CellModel { Text = item, ButtonColorBrush = Color.AliceBlue });
            }

            Assert.IsTrue(mainPage.CheckIfWin("x", mainPage.CollectionToArray(mainPage.Collection)));
        }

        [Test]
        public void CheckIfWinTestVertical()
        {
            string[,] testMatrix;
            testMatrix = new[,]
            {
                {"x", "", ""},
                {"x", "", ""},
                {"x", "", ""}
            };

            mainPage.Collection.Clear();
            foreach (string item in testMatrix)
            {
                mainPage.Collection.Add(new CellModel { Text = item });
            }

            Assert.IsTrue(mainPage.CheckIfWin("x", mainPage.CollectionToArray(mainPage.Collection)));
        }
    }
}