﻿using System;
using System.Drawing;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace TicTacToe.Converters
{
    public class ColorToSolidColorBrush : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value is Color color)
            {
                return new SolidColorBrush(Windows.UI.Color.FromArgb(color.A, color.R, color.G, color.B));
            }

            throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (value is SolidColorBrush solidColorBrush)
            {
                return Windows.UI.Color.FromArgb(solidColorBrush.Color.A, solidColorBrush.Color.R,
                    solidColorBrush.Color.G, solidColorBrush.Color.B);
            }

            throw new NotImplementedException();
        }
    }
}