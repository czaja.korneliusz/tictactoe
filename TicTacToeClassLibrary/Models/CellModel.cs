﻿using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace TicTacToeClassLibrary.Models
{
    public sealed class CellModel : INotifyPropertyChanged
    {
        private Color _buttonColorBrush;

        private bool _notUsed;


        private string _text;

        public string Text
        {
            get => _text;
            set
            {
                _text = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        public Color ButtonColorBrush
        {
            get => _buttonColorBrush;
            set
            {
                _buttonColorBrush = value;
                OnPropertyChanged(nameof(ButtonColorBrush));
            }
        }

        public bool NotUsed
        {
            get => _notUsed;
            set
            {
                _notUsed = value;
                OnPropertyChanged(nameof(NotUsed));
            }
        }


        public ICommand ButtonCommand { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}