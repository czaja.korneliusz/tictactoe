﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using TicTacToeClassLibrary.Models;

namespace TicTacToeClassLibrary.ViewModels
{
    public sealed class MainPageViewModel : INotifyPropertyChanged
    {
        //private CellModel[,] CollectionArray { get; set; }

        private readonly IEnumerable<string> _playerSigns = new[] { "x", "o" };

        private readonly Color _winningColorBrush = Color.FromArgb(255, 200, 255, 180);

        private string _winner;

        private bool _xMove = true;

        public MainPageViewModel()
        {
            Collection = new ObservableCollection<CellModel>();
            StartGame();
            ChangeButtonCommand =
                new RelayCommand(param => StartGame(), () => { return !Collection.Any(x => !x.NotUsed); });
        }

        public ObservableCollection<CellModel> Collection { get; private set; }
        public ICommand ReplayCommand { get; private set; }
        public ICommand ChangeButtonCommand { get; }

        private int BoardSize => (int)Math.Sqrt(Collection.Count);

        public int NumberOfRowsAndColumns { get; set; } = 4;

        public int PointsToWin { get; set; } = 4;

        public double MySize => NumberOfRowsAndColumns * 200 + (Math.Sqrt(Collection.Count)) * 10;

        public string Winner
        {
            get => _winner;
            private set
            {
                _winner = value;
                OnPropertyChanged(nameof(Winner));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void StartGame()
        {
            _xMove = true;
            Winner = null;
            
            Collection.Clear();
            ObservableCollection<CellModel> tempCollection2 = PrepareGameBoardCollection(NumberOfRowsAndColumns);
            foreach (CellModel item in tempCollection2)
            {
                Collection.Add(item);
            }
            OnPropertyChanged(nameof(MySize));
            //ReplayCommand = new RelayCommand(x =>
            //{
            //    Winner = null;
            //    Collection.Clear();
            //    ObservableCollection<CellModel> tempCollection = PrepareGameBoardCollection((int)Math.Pow(NumberOfRowsAndColumns, 2));
            //    foreach (CellModel item in tempCollection)
            //    {
            //        Collection.Add(item);
            //    }

            //});
        }

        private void Test(object param)
        {
            if (param is CellModel cellModel && string.IsNullOrEmpty(cellModel.Text))
            {
                cellModel.Text = _xMove ? _playerSigns.ElementAt(0) : _playerSigns.ElementAt(1);
                _xMove = !_xMove;


                if (Collection.Count(x => x.Text == cellModel.Text) >= PointsToWin)
                {
                    CheckIfWin(cellModel.Text, CollectionToArray(Collection));
                }
            }
        }

        public CellModel[,] CollectionToArray(ObservableCollection<CellModel> collection)
        {
            CellModel[,] collectionArray = new CellModel[BoardSize, BoardSize];
            for (int i = 0; i < Math.Sqrt(collection.Count); i++)
            {
                for (int j = 0; j < Math.Sqrt(collection.Count); j++)
                {
                    collectionArray[j, i] = collection[j + i * BoardSize];
                }
            }

            return collectionArray;
        }


        private ObservableCollection<CellModel> PrepareGameBoardCollection(int howMany)
        {
            ObservableCollection<CellModel> tempCollection = new ObservableCollection<CellModel>();
            for (int j = 0; j < (howMany); j++)
            {
                for (int i = 0; i < (howMany); i++)
                {
                    tempCollection.Add(new CellModel
                    {
                        // Text = $"{i.ToString()},{j.ToString()}",
                        Text = "",
                        ButtonCommand = new RelayCommand(Test, () => Winner == null),
                        NotUsed = true
                    }
                );
                }
            }

            return tempCollection;
        }


        public bool CheckIfWin(string currentPlayerSign, CellModel[,] gameBoardArray)
        {
            for (int currentY = 0; currentY < BoardSize; currentY++) //check up
            {
                for (int currentX = 0; currentX < BoardSize; currentX++)
                {
                    if (CheckVertical(gameBoardArray, currentPlayerSign, currentX, currentY) ||
                        CheckHorizontal(gameBoardArray, currentPlayerSign, currentX, currentY) ||
                        CheckDiagonal(gameBoardArray, currentPlayerSign, currentX, currentY) ||
                        CheckDiagonal2(gameBoardArray, currentPlayerSign, currentX, currentY))
                    {
                        return true;
                    }

                    if (Collection.Count(x => x.Text == _playerSigns.ElementAt(0) || x.Text == _playerSigns.ElementAt(1)) ==
                    Collection.Count)
                    {
                        Winner = "tie";
                        return true;
                    }
                }
            }

            return false;
        }


        private bool CheckVertical(CellModel[,] gameBoardArray, string currentPlayerSign, int currentX, int currentY)
        {
            List<int> indexesList = new List<int>();
            for (int i = 0; i < PointsToWin; i++)
            {
                if (0 + currentX < BoardSize && i + currentY < BoardSize)
                {
                    if (gameBoardArray[0 + currentX, i + currentY].Text == currentPlayerSign)
                    {
                        indexesList.Add(0 + currentX + NumberOfRowsAndColumns * (i + currentY));
                    }
                    else
                    {
                        break;
                    }
                }
            }

            if (indexesList.Count == PointsToWin)
            {
                foreach (int item in indexesList)
                {
                    Collection[item].ButtonColorBrush = _winningColorBrush;
                }

                Winner = currentPlayerSign;
                return true;
            }

            indexesList.Clear();
            return false;
        }


        private bool CheckHorizontal(CellModel[,] gameBoardArray, string currentPlayerSign, int currentX, int currentY)
        {
            List<int> indexesList = new List<int>();
            for (int i = 0; i < PointsToWin; i++)
            {
                if (i + currentX < BoardSize && 0 + currentY < BoardSize)
                {
                    if (gameBoardArray[i + currentX, 0 + currentY].Text == currentPlayerSign)
                    {
                        indexesList.Add(i + currentX + NumberOfRowsAndColumns * (0 + currentY));
                    }
                    else
                    {
                        break;
                    }
                }
            }

            if (indexesList.Count == PointsToWin)
            {
                foreach (int item in indexesList)
                {
                    Collection[item].ButtonColorBrush = _winningColorBrush;
                }

                Winner = currentPlayerSign;
                return true;
            }

            indexesList.Clear();
            return false;
        }


        private bool CheckDiagonal(CellModel[,] gameBoardArray, string currentPlayerSign, int currentX, int currentY)
        {
            List<int> indexesList = new List<int>();
            for (int i = 0; i < PointsToWin; i++)
            {
                if (i + currentX < BoardSize && currentY + i < BoardSize)
                {
                    if (gameBoardArray[i + currentX, i + currentY].Text == currentPlayerSign)
                    {
                        indexesList.Add(i + currentX + NumberOfRowsAndColumns * (i + currentY));
                    }
                    else
                    {
                        break;
                    }
                }
            }

            if (indexesList.Count == PointsToWin)
            {
                foreach (int item in indexesList)
                {
                    Collection[item].ButtonColorBrush = _winningColorBrush;
                }

                Winner = currentPlayerSign;
                return true;
            }

            return false;
        }


        private bool CheckDiagonal2(CellModel[,] gameBoardArray, string currentPlayerSign, int currentX, int currentY)
        {
            List<int> indexesList = new List<int>();
            for (int i = 0; i < PointsToWin; i++)
            {
                if (i + currentX < BoardSize && currentY - i >= 0)
                {
                    if (gameBoardArray[i + currentX, currentY - i].Text == currentPlayerSign)
                    {
                        indexesList.Add(i + currentX + NumberOfRowsAndColumns * (currentY - i));
                    }
                    else
                    {
                        break;
                    }
                }
            }

            if (indexesList.Count == PointsToWin)
            {
                foreach (int item in indexesList)
                {
                    Collection[item].ButtonColorBrush = _winningColorBrush;
                }

                Winner = currentPlayerSign;
                return true;
            }

            return false;
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}